FROM docker.elastic.co/logstash/logstash:6.4.1

ADD ./src/data.csv /var/www/

RUN rm -f /usr/share/logstash/pipeline/logstash.conf
ADD pipeline/ /usr/share/logstash/pipeline/
